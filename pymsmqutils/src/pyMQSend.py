# Filename = pyMQSend.py
# Author = Rudolph Lombaard, prlombaard@gmail.com
# Date First Created = 26 October 2016

import logging           # used for logging
import win32com.client   # Python wrapper that accesses the win32api used for accesing MSMQ API
import pywintypes        # used for "parsing" the Exception that occurs when calling through the win32api. used for debugging
from os import getenv as os_getenv   # used for getting the hostname of the machine where the script is running
import argparse          # used for parsing comandline arguments
import inspect           # used for loggin the function in run-time

# Enable logging
# logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.DEBUG) #uncomment for more basic logging messages
logging.basicConfig(format='%(asctime)s - %(funcName)15s - %(levelname)5s - %(message)s', level=logging.DEBUG)

logger = logging.getLogger(__name__)


def log(func):
    """
    Decorator that loggs when entering and exiting a called function
    """
    def wrapped(*args, **kwargs):
        try:
            logging.debug("ENTERING [%s] with parameters %s" % (func.__name__, args))
            try:
                return func(*args, **kwargs)
            except Exception as e:
                logger.warning("Exception in %s : %s" % (func.__name__, e))
        finally:
            logger.debug("EXITING: [%s]" % func.__name__)
    return wrapped


@log
def send_via_msmq(queue_name, text_to_send, message_label, preferred_priority=3):
    logger.debug("Sending text=[%s] via queue=[%s] with label=[%s] with a priority of=[%d]" % (text_to_send, queue_name, message_label, preferred_priority))
    # Send a message "text_to_send" to private MSMQ "queue_name" to the local computer with default priority=3
    qinfo = win32com.client.Dispatch("MSMQ.MSMQQueueInfo")
    computer_name = os_getenv('COMPUTERNAME')
    # TODO #7: Fix queue name, currently assumes that user have already created a private qeueu that is named myqueue
    qinfo.FormatName = "direct=os:" + computer_name + "\\PRIVATE$\\" + queue_name
    try:
        queue = qinfo.Open(2, 0)   # Open a ref to queue
        msg = win32com.client.Dispatch("MSMQ.MSMQMessage")
        msg.Label = message_label
        msg.Priority = preferred_priority
        msg.Body = text_to_send
        msg.Send(queue)
        queue.Close()
    except pywintypes.com_error as e:
        logger.warning("Something seriously went wrong trying to open the queue. Does it exist?")
        logger.warning(repr(e))
        logger.warning("Message was NOT sent to queue")


@log
def main_send_text(queue_name, message_content_to_send):
    logger.debug("Sending text=[%s] via queue=[%s]" % (message_content_to_send, queue_name))
    send_via_msmq(queue_name, message_content_to_send, "TextMessage")
    logger.info("Text message sent via MSMQ")


@log
def main():
    # Called when script is run as __main__
    # FIXES #8: Add commandline argument parser

    parser = argparse.ArgumentParser()
    parser.add_argument('-s', '--sourceQueuePath')
    parser.add_argument('-b', '--bodyText')
    # TODO 11 #: Add optional argument message priority
    # TODO 12 #: Add optional argument filename
    args = parser.parse_args()
    logger.debug("Parser args %s" % args)
    if args.sourceQueuePath is not None:
        # Assume that only one of two arguments can be present either bodyText or filename but not both
        # TODO 12 #: Send either text or contents of file
        if args.bodyText is not None:
            logger.debug("Received arguments..sourcePathQueue and bodytext")
            logger.info("User asked to send text")
            main_send_text(args.sourceQueuePath, args.bodyText)
        else:
            logger.info("No body text supplied cannot send nothing to queue %s!!" % args.bodyText)
            parser.print_help()
    else:
        # Arguments from the commandline is not correct print usage
        logger.debug("Something went wrong with parsing arguments..printing usage")
        parser.print_help()


if __name__ == "__main__":
    main()
