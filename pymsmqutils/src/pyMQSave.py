# Filename = pyMQSave.py
# Author = Rudolph Lombaard, prlombaard@gmail.com
# Date First Created = 26 October 2016

import logging
import win32com.client
import os

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.DEBUG)

logger = logging.getLogger(__name__)


def receive_via_msmq():
    # Receives a message via MSMQ
    qinfo = win32com.client.Dispatch("MSMQ.MSMQQueueInfo")
    computer_name = os.getenv('COMPUTERNAME')
    # TODO: Fix queue name, currently assumes that user have already created a private qeueu that is named myqueue
    qinfo.FormatName = "direct=os:" + computer_name + "\\PRIVATE$\\myqueue"
    queue = qinfo.Open(1, 0)   # Open a ref to queue to read(1)

    # TODO #10 : Change behaviour so that receiving a message does not block, maybe a new script all together?
    received_msg = queue.Receive()     # This is a blocking call, if there are no mewssages on queue this blocks
    queue.Close()
    return received_msg


def main_recv():
    logger.info("Trying to retrieve message")
    msg_received = receive_via_msmq()
    logger.info("Message probably retrieved it follows")

    # TODO #9 : Display to screen or save the message to a file depending on the message Label
    logger.info("Label:" % (msg_received.Label))
    logger.info("Body :" % (msg_received.Body))


def main():
    # Called when script is run as __main__
    # TODO #9: Add commandline argument parser
    logger.info("pyMQSave.py")

    main_recv()


if __name__ == "__main__":
    main()
